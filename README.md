# The Blog

A simple and stylish blog.

## Installation
*Note: If you are following these instructions when deploying the website, it is **highly** recommended that you clone the repository in `/srv`.*
- Clone this repository.
- Change the directory to `the-blog`.
- Create a virtual environment using `$ python3 -m venv venv`.
- Activate virtual environment using `$ source venv/bin/activate`.
- Install the `wheel` package using `$ pip install wheel`
- Install the dependencies using `$ pip install -r requirements.txt`.
- Apply the migrations using `$ python manage.py migrate`.
- Create a superuser account using `$ python manage.py createsuperuser`.
- Load the initial data using `$ python manage.py loaddata inital_data.json`
- Run the server using `$ python manage.py runserver`.

You should now have a development version of the blog accessible at `localhost:8000` or `127.0.0.1:8000`.

## Deployment
*Follow the installation instructions before continuing. If you are running the Django server, press Control-C to close it.*

*Make sure you have `root` privileges.*

### Django configuration
- Collect the static files (by default in `/var/www/the-blog/`) using `$ python manage.py collectstatic`.
- Generate a `SECRET_KEY` using `$ python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'`.
Copy the returned key to your clipboard because we will need it soon.
- Open `common/settings.py` for editing using your preferred text editor.
- Change `DEBUG` to `False`.
- Add your fully qualified domain name to `ALLOWED_HOSTS`.
- Replace the current `SECRET_KEY` with the one you generated earlier (it's in your clipboard, right!?).
- Save and exit.

### Firewall configuration
**If you have a firewall set up (recommended), make sure to open ports 80 and 443.**

- If you have `UFW` set up:
  - Run `$ ufw allow http`.
  - Run `$ ufw allow https`.

- If don't have `UFW` and have only `iptables` set up:
  - Open `/etc/sysconfig/iptables` for editing using your preferred text editor.
  - Add the following lines to the file if they do not already exist, then save and exit:
```
-A INPUT -m state --state NEW -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -p tcp --dport 443 -j ACCEPT
```

### Installing additional dependencies
- Install Gunicorn using `$ pip install gunicorn`.
- Install Nginx using `$ apt install nginx`.
- Install Certbot using `$ apt install python3-certbot-nginx`.

### Setting up the Gunicorn service
- Open `/etc/systemd/system/gunicorn_blog.socket` for editing using your preferred text editor.
- Add the following to the file, then save and exit:
```
[Unit]
Description=gunicorn blog socket

[Socket]
ListenStream=/run/gunicorn_blog.sock

[Install]
WantedBy=sockets.target
```
- Open `/etc/systemd/system/gunicorn_blog.service` for editing using your preferred text editor.
- Add the following to the file:
```
[Unit]
Description=gunicorn blog daemon
Requires=gunicorn_blog.socket
After=network.target

[Service]
User=www-data
Group=www-data
WorkingDirectory=/srv/the-blog
ExecStart=/srv/the-blog/venv/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn_blog.sock \
          common.wsgi:application

[Install]
WantedBy=multi-user.target
```
- Start the Gunicorn socket using `$ systemctl start gunicorn_blog.socket`.
- Enable the Gunicorn socket (run at startup) using `$ systemctl enable gunicorn_blog.socket`.

### Setting up Nginx
- Remove the `default` configuration from `sites-enabled` using `$ rm /etc/nginx/sites-enabled/default`.
- Open `/etc/nginx/sites-available/blog` for editing using your preferred text editor.
- Add the following to the file, then save and exit:
```
server {
    listen 80;
    server_name YOUR_FULLY_QUALIFIED_DOMAIN_NAME;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /var/www/the-blog;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn_blog.sock;
    }
}
```
- Activate the Nginx config using `$ ln -s /etc/nginx/sites-available/blog /etc/nginx/sites-enabled/blog`.
- Restart Nginx `$ systemctl restart nginx`.

### Setting up the HTTPS certificate
- Create an HTTPS certificate with Certbot using `$ certbot --nginx -d YOUR_FULLY_QUALIFIED_DOMAIN_NAME`.
- Follow the script instructions.
- You should choose option 2 (Redirect) when the script asks if you want users to be redirected to the `HTTPS` version of the website if they try accessing the `HTTP` version.

Good job! You should now have a running instance of the blog.

## Updating
You can easily update your running blog instance.

- Pull the newest updates from Git using `$ git pull`.
  - *Fix any merge conflicts that might arise.*
- If you are running on your own branch, merge the changes from `master` using `$ git merge origin/master`.
- If there are any model changes:
  - Run `$ python manage.py migrate`.
- If there are any static files changes:
  - Run `$ python manage.py collectstatic`.
  - Run `$ sudo systemctl restart nginx`.
- Run `$ sudo systemctl restart gunicorn_blog.socket`.
