from django import template
from django.conf import settings
from django.utils.encoding import smart_str, force_text
from django.utils.safestring import mark_safe
from docutils.core import publish_parts

register = template.Library()


@register.filter(is_safe=True)
def restruct(value):
    """
    :type value: str
    :rtype: str
    """
    docutils_settings = getattr(settings, "RESTRUCTUREDTEXT_FILTER_SETTINGS", {})
    parts = publish_parts(source=smart_str(value), writer_name="html4css1", settings_overrides=docutils_settings)
    content = force_text(parts["fragment"])
    for h in range(5, 0, -1):
        content = content.replace(f"<h{h}>", f"<h{h + 1}>")
        content = content.replace(f"</h{h}>", f"</h{h + 1}>")
    return mark_safe(content)
