from datetime import date
from django.views import View
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, reverse
from django.forms.models import modelform_factory
from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from next_prev import next_in_order, prev_in_order
from blog.forms import PostForm, UploadForm
from blog.models import Post, Tag, Upload


class SearchView(View):
    def get(self, request):
        tag = request.GET.get("tag", None)
        year = request.GET.get("year", None)
        if request.user.is_authenticated:
            if tag:
                tag_obj = get_object_or_404(Tag, name=tag)
                posts = tag_obj.posts.all().distinct()
            elif year and year.isnumeric():
                posts = Post.objects.filter(created_at__year=year)
            else:
                search = request.GET.get("search", "")
                posts = Post.objects.filter(
                    Q(title__contains=search) | Q(tags__name__contains=search) | Q(content__contains=search)
                ).distinct()
            return render(request=request, template_name="blog/search.html", context={"posts": posts})
        else:
            if tag:
                tag_obj = get_object_or_404(Tag, name=tag)
                posts = tag_obj.posts.filter(is_published=True).distinct()
            elif year and year.isnumeric():
                posts = Post.objects.filter(created_at__year=year).filter(is_published=True)
            else:
                search = request.GET.get("search", "")
                posts = (
                    Post.objects.filter(
                        Q(title__contains=search) | Q(tags__name__contains=search) | Q(content__contains=search)
                    )
                    .filter(is_published=True)
                    .distinct()
                )
            return render(request=request, template_name="blog/search.html", context={"posts": posts})


class TagsView(View):
    def get(self, request):
        tags = Tag.objects.filter(posts__isnull=False).distinct()
        return render(request=request, template_name="blog/tags.html", context={"tags": tags})


class CreateView(LoginRequiredMixin, View):
    def get(self, request):
        form = PostForm()
        return render(request=request, template_name="blog/create.html", context={"form": form})

    def post(self, request):
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save()
            for tag_name in form.cleaned_data["tags_list"].split(","):
                tag = Tag.objects.get_or_create(name=tag_name)
                post.tags.add(tag[0].pk)
            post.save()
            messages.success(request, "You have successfully created a new blog post.")
            return HttpResponseRedirect(reverse("index"))
        return render(request=request, template_name="blog/create.html", context={"form": form})


class PostView(View):
    def get(self, request, pk=None):
        post = get_object_or_404(Post, pk=pk)
        return render(
            request=request,
            template_name="blog/post.html",
            context={"post": post, "prev": prev_in_order(post), "next": next_in_order(post), "date": date.today()},
        )


class EditView(LoginRequiredMixin, View):
    def get(self, request, pk=None):
        post = get_object_or_404(Post, pk=pk)
        tags_list = ",".join([tag.name for tag in set(post.tags.all())])
        form = PostForm(instance=post, initial={"tags_list": tags_list})
        return render(request=request, template_name="blog/edit.html", context={"form": form})

    def post(self, request, pk=None):
        post = get_object_or_404(Post, pk=pk)
        tags_list = ",".join([tag.name for tag in set(post.tags.all())])
        form = PostForm(request.POST, instance=get_object_or_404(Post, pk=pk), initial={"tags_list": tags_list})
        if form.is_valid():
            post = form.save()
            post.tags.clear()
            for tag_name in form.cleaned_data["tags_list"].split(","):
                tag = Tag.objects.get_or_create(name=tag_name)
                post.tags.add(tag[0].pk)
            post.save()
            messages.success(request, "You have successfully edited a blog post.")
            return HttpResponseRedirect(reverse("index"))
        return render(request=request, template_name="blog/edit.html", context={"form": form})


class DeleteView(LoginRequiredMixin, View):
    def get(self, request, pk=None):
        return render(request=request, template_name="blog/delete.html")

    def post(self, request, pk=None):
        get_object_or_404(Post, pk=pk).delete()
        messages.success(request, "You have successfully deleted a blog post.")
        return HttpResponseRedirect(reverse("index"))


@method_decorator(csrf_exempt, name="dispatch")
class RenderMarkdown(LoginRequiredMixin, View):
    def post(self, request):
        return render(
            request=request, template_name="blog/render.html", context={"content": request.POST.get("content", "")}
        )


class UploadView(LoginRequiredMixin, View):
    def get(self, request):
        uploads = Upload.objects.all()
        return render(self.request, "blog/upload_list.html", {"uploads": uploads})

    def post(self, request):
        form = UploadForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            upload = form.save()
            data = {"is_valid": True, "name": upload.file.name, "url": upload.file.url}
        else:
            data = {"is_valid": False}
        return JsonResponse(data)


class UploadDeleteView(LoginRequiredMixin, View):
    def get(self, request, pk=None):
        get_object_or_404(Upload, pk=pk)
        return render(request=request, template_name="blog/upload_delete.html")

    def post(self, request, pk=None):
        get_object_or_404(Upload, pk=pk).delete()
        messages.success(request, "You have successfully deleted a file.")
        return HttpResponseRedirect(reverse("blog:upload"))
