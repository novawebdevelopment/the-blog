from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=32, unique=True)

    def get_html_count(self):
        return self.posts.count() * 6 + 12


class Post(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    tags = models.ManyToManyField(Tag, related_name="posts", blank=True)
    created_at = models.DateField(help_text="Must be formatted as YYYY-MM-DD")
    is_published = models.BooleanField(default=False)

    class Meta:
        ordering = ["-created_at"]


class Upload(models.Model):
    file = models.FileField(upload_to="uploads/")
    created_at = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ["-created_at"]
