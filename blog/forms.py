from datetime import date
from django import forms
from blog.models import Post, Upload


class PostForm(forms.ModelForm):
    tags_list = forms.CharField(label="Tags", required=False)

    class Meta:
        model = Post
        exclude = ("tags",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["created_at"].initial = date.today()


class UploadForm(forms.ModelForm):
    class Meta:
        model = Upload
        fields = "__all__"
