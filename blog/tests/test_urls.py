from django.test import TestCase
from django.urls import resolve


class BoxesUrlsCase(TestCase):
    def test_list(self):
        """Check list URL correct"""
        self.assertEqual(resolve("/blog/").view_name, "blog:list")

    def test_tags(self):
        """Check tags URL correct"""
        self.assertEqual(resolve("/blog/tags/").view_name, "blog:tags")

    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/blog/create/").view_name, "blog:create")

    def test_post(self):
        """Check post URL correct"""
        self.assertEqual(resolve("/blog/1/").view_name, "blog:post")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/blog/1/edit/").view_name, "blog:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/blog/1/delete/").view_name, "blog:delete")

    def test_render(self):
        """Check render URL correct"""
        self.assertEqual(resolve("/blog/render/").view_name, "blog:render")

    def test_upload(self):
        """Check upload URL correct"""
        self.assertEqual(resolve("/blog/upload/").view_name, "blog:upload")

    def test_upload_delete(self):
        """Check upload delete URL correct"""
        self.assertEqual(resolve("/blog/upload/1/delete/").view_name, "blog:upload-delete")
