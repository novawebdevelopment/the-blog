from django.test import TestCase
from boxes.apps import BoxesConfig


class BoxesAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(BoxesConfig.name, "boxes")
