from django.http import HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render, get_object_or_404, reverse
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from boxes.models import Box
from boxes.forms import BoxForm


class EditView(LoginRequiredMixin, View):
    def get(self, request, pk=None):
        box = get_object_or_404(Box, pk=pk)
        form = BoxForm(instance=box)
        return render(request=request, template_name="boxes/edit.html", context={"form": form})

    def post(self, request, pk=None):
        box = get_object_or_404(Box, pk=pk)
        form = BoxForm(request.POST, instance=box)
        if form.is_valid():
            form.save()
            messages.success(request, "The box has been successfully edited.")
            return HttpResponseRedirect(reverse("index"))
        return render(request=request, template_name="boxes/edit.html", context={"form": form})
