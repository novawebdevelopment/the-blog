from django.db import models


class Box(models.Model):
    uid = models.AutoField(primary_key=True)
    content = models.TextField()
