from django.contrib.auth.base_user import BaseUserManager


class AccountManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, first_name, last_name):
        if not email:
            raise ValueError("A unique email is required for each user.")
        user = self.model(email=self.normalize_email(email.lower()), first_name=first_name, last_name=last_name)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email, password, first_name, last_name, **extra_fields):
        return self._create_user(email, password, first_name, last_name)

    def create_superuser(self, email, password, first_name, last_name, **extra_fields):
        return self._create_user(email, password, first_name, last_name)
