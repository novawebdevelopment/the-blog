from django.test import TestCase
from account.apps import AccountConfig


class AccountAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(AccountConfig.name, "account")
