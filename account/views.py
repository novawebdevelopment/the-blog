from django.views import View
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
from account.forms import *

class LoginView(View):
    def dispatch(self, request):
        self.next = request.GET.get("next", None)
        if not self.next or "://" in self.next or " " in self.next:
            self.next = "/"
        if request.user.is_authenticated:
            messages.warning(request, "You are already logged in!")
            return HttpResponseRedirect(self.next)
        return super().dispatch(request)

    def get(self, request):
        form = LoginForm()
        return render(request=request, template_name="account/login.html", context={"form": form})

    def post(self, request):
        form = LoginForm(request.POST)
        if form.login(request):
            messages.success(request, f"Welcome back, {request.user.first_name}! You have successfully logged in.")
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="account/login.html", context={"form": form})


class LogoutView(View):
    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
            messages.success(request, "You have successfully logged out.")
        else:
            messages.warning(request, "You are already logged out!")
        return HttpResponseRedirect(reverse("account:login"))


class PasswordChangeView(LoginRequiredMixin, View):
    def get(self, request):
        form = PasswordChangeForm()
        return render(request=request, template_name="account/password/change.html", context={"form": form})

    def post(self, request):
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            form.save(request.user)
            messages.add_message(request, messages.SUCCESS, "You have successfully changed your password.")
            return HttpResponseRedirect(reverse("account:login"))
        return render(request=request, template_name="account/password/change.html", context={"form": form})


class AccountView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request=request, template_name="account/account.html", context={"account": request.user})


class EditView(LoginRequiredMixin, View):
    def get(self, request):
        form = AccountForm(instance=request.user, initial={"verify_email": request.user.email})
        return render(request=request, template_name="account/edit.html", context={"form": form})

    def post(self, request):
        form = AccountForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, "The account has been successfully edited.")
            return HttpResponseRedirect(reverse("account:account"))
        return render(request=request, template_name="account/edit.html", context={"form": form})
