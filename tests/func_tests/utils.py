from importlib import import_module
from django.conf import settings
from django.contrib.auth import SESSION_KEY, BACKEND_SESSION_KEY, HASH_SESSION_KEY
from selenium import webdriver
from account.models import Account


def setup_browser():
    options = webdriver.ChromeOptions()
    #options.set_headless()
    options.accept_insecure_certs = True
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-gpu")
    browser = webdriver.Chrome(options=options)
    browser.implicitly_wait(3)
    return browser


def setup_account():
    return Account.objects.create_superuser(
        "selenium@test.user", "STP12345", first_name="Selenium-Test", last_name="User"
    )


def force_login(user, browser, base_url):
    SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
    browser.get("{}{}".format(base_url, "/"))

    session = SessionStore()
    session[SESSION_KEY] = user.uid
    session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
    session[HASH_SESSION_KEY] = user.get_session_auth_hash()
    session.save()

    cookie = {
        "name": settings.SESSION_COOKIE_NAME,
        "value": session.session_key,
        "path": "/",
    }
    browser.add_cookie(cookie)
    browser.refresh()
