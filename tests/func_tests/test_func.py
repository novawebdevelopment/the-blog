import time
from django.core import mail
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from account.models import Account
from utils import setup_browser, setup_account, force_login


class AccountsTest(StaticLiveServerTestCase):
    fixtures = ["initial_data.json", "tests/test_data/blogs.json"]

    def setUp(self):
        self.browser = setup_browser()
        self.account = setup_account()

    def tearDown(self):
        self.browser.quit()

    def test_login_account(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        self.browser.get(self.live_server_url + "/")

        self.browser.find_element_by_xpath('//a[@href="/account/login/"]').click()
        assert "The Blog" in self.browser.title

        self.browser.find_element_by_name("email").send_keys("selenium@test.user")
        self.browser.find_element_by_name("password").send_keys("STP12345")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert (
            "Welcome back, Selenium-Test! You have successfully logged in."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

        self.browser.get(self.live_server_url + "/account/logout/")

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert "You have successfully logged out." in self.browser.find_element_by_class_name("alert-dismissible").text


    def test_change_password_account(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/account/password/change/")

        assert "The Blog" in self.browser.title
        self.browser.find_element_by_name("new_password").send_keys("NEW_STP12345")
        self.browser.find_element_by_name("verify_new_password").send_keys("NEW_STP12345")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.75)
        assert (
            "You have successfully changed your password."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )


    def test_detail_account(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/account/")

        assert "The Blog" in self.browser.title
        assert "Selenium-Test" in self.browser.find_element_by_class_name("table").text
        assert "User" in self.browser.find_element_by_class_name("table").text
        assert "selenium@test.user" in self.browser.find_element_by_class_name("table").text

    def test_edit_account(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/account/")

        self.browser.find_element_by_xpath(
            f'//a[@href="/account/edit/"]'
        ).click()

        assert "The Blog" in self.browser.title
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        time.sleep(0.5)
        assert (
            "The account has been successfully edited."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_edit_boxes(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + "/")

        self.browser.get(self.live_server_url + "/about_me/")
        self.browser.find_element_by_xpath('//a[@href="/boxes/4/?next=/about_me/"]').click()
        
        self.browser.find_element_by_name("content").send_keys("This is a title")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert (
            "The box has been successfully edited."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )
    
    def test_blog_create(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + "/")

        self.browser.find_element_by_xpath('//a[@href="/blog/create/"]').click()
        
        self.browser.find_element_by_name("title").send_keys("This is a title")
        self.browser.find_element_by_name("content").send_keys("This is content")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element_by_name("is_published").click()
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert (
            "You have successfully created a new blog post."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )


    def test_blog_edit(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + "/")

        # Navigate to Edit Blog Post
        self.browser.find_element_by_xpath('//a[@href="/blog/"]').click()
        self.browser.find_element_by_xpath('//a[@href="/blog/1/"]').click()
        self.browser.find_element_by_xpath('//a[@href="/blog/1/edit/"]').click()

        self.browser.find_element_by_name("title").send_keys("This is a title")
        self.browser.find_element_by_name("content").send_keys("This is content")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element_by_name("is_published").click()
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert (
            "You have successfully edited a blog post."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_blog_delete(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + "/")

        # Navigate to Edit Blog Post
        self.browser.find_element_by_xpath('//a[@href="/blog/"]').click()
        self.browser.find_element_by_xpath('//a[@href="/blog/1/"]').click()
        self.browser.find_element_by_xpath('//a[@href="/blog/1/delete/"]').click()

        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert (
            "You have successfully deleted a blog post."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_blog_search(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + "/")

        # Navigate to Edit Blog Post
        self.browser.find_element_by_xpath('//a[@href="/blog/"]').click()
        self.browser.find_element_by_name("search").send_keys("test")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert (
            "test"
            in self.browser.find_element_by_class_name("badge").text
        )
    
    def test_blog_year(self):
        fixtures = ["initial_data.json", "tests/test_data/blogs.json"]
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + "/")

        # Navigate to Edit Blog Post
        self.browser.find_element_by_xpath('//a[@href="/blog/?year=2020"]').click()

        assert "The Blog" in self.browser.title
        time.sleep(0.5)
        assert (
            "test"
            in self.browser.find_element_by_class_name("badge").text
        )